package ru.t1.chernysheva.tm.command.project;

import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-clear";

    @NotNull
    private final String DESCRIPTION = "Delete all projects.";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull String userId = getUserId();
        getProjectService().clear(userId);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }


}
