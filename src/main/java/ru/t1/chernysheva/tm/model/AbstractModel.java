package ru.t1.chernysheva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class AbstractModel {

    @NotNull
    private String id = UUID.randomUUID().toString();

}
